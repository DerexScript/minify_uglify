const UglifyJS = require("uglify-es");
const fs = require('fs');



let deleteFolderRecursive = path => {
	if (fs.existsSync(path)) {
		fs.readdirSync(path).forEach(file => {
			let curPath = path + "/" + file;
			if (fs.lstatSync(curPath).isDirectory()) {
				deleteFolderRecursive(curPath);
			} else {
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};

(async () => {
	await deleteFolderRecursive("./output/");

	if (!fs.existsSync("./input/")) {
		fs.mkdirSync("./input/");
	}
	if (!fs.existsSync("./output/")) {
		fs.mkdirSync("./output/");
	}
	let filenames;
	try {
		filenames = fs.readdirSync("./input/");
	} catch (e) {
		if (e.errno == -4058) {
			console.log("Diretorio Não Existe!");
		} else {
			console.log("Erro Desconhecido Ao Ler Diretorio!");
		}
	}
	if (filenames.length > 0) {
		let filesJsminified = filenames.map(elem => {
			return UglifyJS.minify(fs.readFileSync(`./input/${elem}`, "utf8"), { mangle: { toplevel: true } }).code;
		});
		filesJsminified.forEach((elem, index) => {
			fs.writeFileSync(`./output/${filenames[index]}`, elem, "utf8");
		});
	} else {
		console.log("Diretorio Vazio!");
	}
})();

